package com.example.u6166704.lab2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Counter extends AppCompatActivity {
    Integer numClicks = 0;
    Long timeStart = -1L;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_counter);
    }

    public void addCounter(View v) {

        TextView counterText = (TextView) findViewById(R.id.counter_text);
        Integer count = new Integer((String) counterText.getText());
        counterText.setText(((Integer)(count+1)).toString());

        if(timeStart == -1) {
            timeStart = System.currentTimeMillis();
        }

        numClicks += 1;
    }

    public void resetCounter(View v) {

        TextView counterText = (TextView) findViewById(R.id.counter_text);
        counterText.setText("0");

        TextView rateText = (TextView) findViewById(R.id.text_rate);

        if(timeStart == -1) {
            rateText.setText("");
        } else {
            Long currentTime = System.currentTimeMillis();
            currentTime -= timeStart;
            currentTime /= 1000L;
            Long cps = numClicks/currentTime;

            rateText.setText("Click rate : "+String.valueOf(cps)+" clicks/sec");
            timeStart = -1L;
            numClicks = 0;

        }



    }

    public void switchToCounter(View v) {

        Intent intent = new Intent(getApplicationContext(), Calculator.class);
        startActivity(intent);

    }


}
