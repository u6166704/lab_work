package com.example.u6166704.lab2;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Calculator extends AppCompatActivity {

    private static Integer att = new Integer(0);
    private static String operation = "";
    public static final String PLUS_OP = "+";
    public static final String MINUS_OP = "-";
    public static final String EQUAL_OP = "=";

    public static final String FILE_NAME= "calculator.dat";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);
    }

    public void switchToCounter(View v) {
        Intent intent = new Intent(getApplicationContext(), Counter.class);
        startActivity(intent);
    }

    public void numericButtonClick(View v) {
        Button clickedButton = (Button) findViewById(v.getId());
        String buttonText = (String) clickedButton.getText();

        TextView textField = (TextView) findViewById(R.id.calc_text);
        String text = (String)textField.getText();
        if(text == null || text.equals("0")) {
            text = "";
        }
        textField.setText((String)(text + buttonText));
    }

    public void saveTextToFile(String text) {
        FileOutputStream outputStream = null;
        try {
            outputStream = getApplicationContext().openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
            outputStream.write(text.getBytes());

        } catch(IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(outputStream != null) outputStream.close();
            } catch (IOException e){}

        }
    }
    public void readTextFromFile() {
        File file = new File(getApplicationContext().getFilesDir(), FILE_NAME);
        int length = (int) file.length();
        byte[] bytes = new byte[length];

        FileInputStream in = null;
        try {
            in = new FileInputStream(file);
            in.read(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(in != null) in.close();
            } catch (IOException e) {}
        }
    }
    public void operationButtonClick(View v) {
        Button clickedButton = (Button) findViewById(v.getId());
        String buttonText = (String) clickedButton.getText();

        TextView textField = (TextView) findViewById(R.id.calc_text);
        String text = (String)textField.getText();
        Integer currentVal = Integer.parseInt(text);

        if(buttonText.equals(PLUS_OP)) {

            att = currentVal;
            operation = buttonText;
            textField.setText(String.valueOf(0));

        } else if(buttonText.equals(MINUS_OP)) {

            att = currentVal;
            operation = buttonText;
            textField.setText(String.valueOf(0));

        } else if(buttonText.equals(EQUAL_OP)) {

            if(operation.equals(PLUS_OP)) {

                att += currentVal;

            } else if (operation.equals(MINUS_OP)) {

                att -= currentVal;

            } else {

                att = 0;

            }

            textField.setText(String.valueOf(att));
            saveTextToFile(String.valueOf(att));

            operation = "";

        }



    }
}
