/*
 * You are not allowed to modify this file!
 */

import java.util.Random;

public class PointGuesser {
  private Point answer;
  private Random rng;

  public PointGuesser() {
    rng = new Random();
    answer = new Point(rng.nextInt(10000), rng.nextInt(10000));
  }

  // Return value:
  // -2 You need to go up
  // -1 You need to go left
  // 0  You guessed correctly!
  // 1  You need to go right
  // 2  You need to go down
  public int guess(Point p) {
    if (rng.nextBoolean()) {
      return m(cx(p), cy(p));
    } else {
      return m(cy(p), cx(p));
    }
  }

  private int cx(Point p) {
    return Integer.compare(answer.x, p.x);
  }

  private int cy(Point p) {
    return 2 * Integer.compare(answer.y, p.y);
  }

  private int m(int a, int b) {
    return (a == 0) ? b : a;
  }
}
