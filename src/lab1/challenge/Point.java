public class Point {

  // Positive x means right, negative x means left
  // Positive y means down, negative y means up
  public int x;
  public int y;

  public Point(int x, int y) {
    this.x = x;
    this.y = y;
  }
}
