import java.util.Random;
import java.lang.String;

public class GuessingGame {

	public static void main(String[] args) {
		NumberGuesser numberGuesser = new NumberGuesser();
		boolean guessedCorrectly = false;
		int guess = 0;
		int max = 9999;
		int min = 0;
		
		/* First, try to guess the number using numberGuesser's guess() method.
		 * It doesn't matter how you do it, or how long your program takes.
		 * The number to guess will always be between 0 and 9999 inclusive.
		 *
		 * Hint: Read the source file for NumberGuesser to find out more about it.
		 */
		
		// Your code goes here
		int i = 0;
		for(i =0; (guess<max && numberGuesser.guess(guess)!=0); guess++, i++);
		System.out.println("Took "+i+" iterations");
		System.out.println("Correctly guessed :"+guess);
		
		
		/*
		 * Then, complete at least two of the following:
		 */
		
		/* 1) Make your program take the least amount of guesses possible.
		 *    Ideally, it should never take more than 14 guesses.
		 */
		
		
		NumberGuesser numberGuesserFast = new NumberGuesser();
		
		// Your code goes here
		guess = 5000;
		max = 9999;
		min = 0;
		int outcome = 0;
		for(i=0; !guessedCorrectly; i++) {
			guess = (max - min)/2 + min;
			outcome = numberGuesserFast.guess(guess);
			if(outcome < 0) {
				max = guess - 1;
			} else if(outcome >0) {
				min = guess + 1;
			} else {
				guessedCorrectly = true;
			}			
		}
		
		System.out.println("Took "+i+" iterations");
		System.out.println("Correctly guessed :"+guess);
		
		/*
		 * 2) Rewrite your code using only recursion.
		 *    No "for" or "while" loops allowed!
		 *    You can use the guessNumberRecursive function below as a starting point.
		 *    Hint: You'll probably want to add parameters to its signature.
		 */
		
		NumberGuesser numberGuesserRecursive = new NumberGuesser();

		max = 9999;
		min = 0;
		
		guessNumberRecursive(numberGuesserRecursive, max, min);
		
		/*
		 * 3) Guess a point in cartesian space, with integer coordinates.
		 *    Hint: Read the source files for Point and PointGuesser to find out more.
		 */
		
		PointGuesser pointGuesser = new PointGuesser();
		guessPointRecursive(pointGuesser, 9999, 0, 9999, 0);
	}

	public static void guessNumberRecursive(NumberGuesser numberGuesser, int max, int min) {
		guessNumberRecursive(numberGuesser, max, min, 0);
	}

	public static void guessNumberRecursive(NumberGuesser numberGuesser, int max, int min, int n) {
		int guess = (max - min)/2 + min;
		int outcome = numberGuesser.guess(guess);
		if(outcome < 0) {
			guessNumberRecursive(numberGuesser, guess, min, n+1);
		} else if(outcome > 0) {
			guessNumberRecursive(numberGuesser, max, guess, n+1);
		} else {
			System.out.println("Took "+n+" recursive calls");
			System.out.println("Correctly guessed :"+guess);
		}
		
	}

	public static Point guessPointRecursive(PointGuesser pointGuesser, int xMax, int xMin, int yMax, int yMin) {
		int x = (xMax - xMin)/2 + xMin;
		int y = (yMax - yMin)/2 + yMin;

		Point point = new Point(x, y);

		int outcome = pointGuesser.guess(point);
		if(outcome==0) {
			System.out.println("Correctly guessed point "+point.x +","+point.y);
		} else if(outcome%2==0) {
			if(outcome<0) {
				guessPointRecursive(pointGuesser, xMax, xMin, y, yMin);
			} else {
				guessPointRecursive(pointGuesser, xMax, xMin, yMax, y);
			}
		} else {
			if(outcome<0) {
				guessPointRecursive(pointGuesser, x, xMin, yMax, yMin);
			} else {
				guessPointRecursive(pointGuesser, xMax, x, yMax, yMin);
			}
		}
		return point;
	}
}
