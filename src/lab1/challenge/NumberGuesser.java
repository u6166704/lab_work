/*
 * You are not allowed to modify this file!
 */

import java.util.Random;

public class NumberGuesser {
  private int answer;

  public NumberGuesser() {
    Random rng = new Random();
    answer = rng.nextInt(10000);
  }

  // Return value:
  // -1 You need to go lower
  // 0  You guessed correctly!
  // 1  You need to go higher
  public int guess(int n) {
    return Integer.compare(answer, n);
  }
}
