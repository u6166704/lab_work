package lab1.problem2;

public class DemoTextDraw {

	/**
	 * Prints a box and then a triangle of fixed dimension
	 */
	public static void main(String[] args) {
		TextDraw box = new Box();
		TextDraw tri = new Triangle();
		box.draw();
		System.out.println("-------------------");
		tri.draw();
	}

}
