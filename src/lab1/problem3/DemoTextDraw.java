package lab1.problem3;

import java.util.Scanner;

public class DemoTextDraw {
	/**
	 * Takes input about width from user
	 * and prints the quadrilateral.
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.print("Size of quad : ");
		Scanner s = new Scanner(System.in);
		int i = s.nextInt();
		
		Quadrilateral q = new Quadrilateral();
		q.draw(i);
		
		s.close();
	}

}
