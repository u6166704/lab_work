package lab1.problem3;

public class Quadrilateral {
	
	void draw(int n) {
		
		int mid = (n/2);
		
		for(int i = 0 ; i < n ; i++) {
			
			// Fix to skip mid line for even width quads
			if(n%2==0 && i==mid) {
				continue;
			}
			
			for(int j = 0; j < n; j++) {				
				if(i<=mid && j>=mid-i && j<=mid+i) {
					System.out.print("#");
				} else if(i>mid && j>=mid-(n-i-1) && j<=mid+(n-i-1)) {
					System.out.print("#");
				} else {
					System.out.print(" ");
				}				
			}
			System.out.println("");
		}
	}


}
