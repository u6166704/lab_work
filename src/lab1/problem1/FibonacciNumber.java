package lab1.problem1;

public class FibonacciNumber {
	
	/**
	 * Gets slow for n > 45 , i.e f(45) 
	 * @param n
	 * @return
	 */
	public static int f(int n) {
		int result = 0;
		
		if(n==0 || n==1) {
			result = n;
		} else {
			result = f(n-1) + f(n-2);
		}		
		
		return result;		
	}
	
	public static void main(String args[]) {
		System.out.println(f(6));
	}
}
