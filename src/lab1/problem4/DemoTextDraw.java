package lab1.problem4;

public class DemoTextDraw {

	/**
	 * Draws a box then a triangle of fixed dimension
	 * @param args
	 */
	public static void main(String[] args) {
		Shape box = new Box();
		Shape tri = new Triangle();
		box.draw();
		System.out.println("-------------------");
		tri.draw();
	}
	
	/*
	 * Difference between problem 4 and problem 3 is that 
	 * the draw method accepts an argument which decides the
	 * size of the shape
	 * 
	 * Also Quadrilateral does not extend a base class
	 */

}
